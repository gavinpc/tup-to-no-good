local file = io.open('list-of-years.txt', 'r')
if file ~= nil then
  for line in file:lines() do
    local wiki, iso = line:match'(.*) (.*)'
    if wiki and iso then
      tup.definerule{
        command = "echo I have looked at "..wiki.." for "..iso.." > %o",
        outputs = {'processed/'..wiki}
      }
    else
      io.write("line ‘"..line.."’\n")
      if wiki == nil then io.write("- no wiki\n") end
      if iso == nil then io.write("- no iso\n") end
    end
  end
  file:close()
else
  -- no such file or couldn't open for read
end
