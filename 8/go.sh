#!/bin/sh

# In this episode, we help tup reach a fixpoint
#
# This appears trivial to do using `tup refactor` because “If any Tupfiles that
# are parsed result in changes to the database, these are reported as errors”.
# But it's not quite the same.  refactor can pass even when there's work to do.
#
# And I was looking at graph.
#
# But duh, tup todo

set -e

is_tup_done_via_refactor() {
    tup refactor > /dev/null 2>&1
}

EMPTY_GRAPH='digraph G {
}'

is_tup_done_via_graph() {
    local dot="$(tup graph)"
    >&2 echo got dot "$dot"
    >&2 echo and empty is "$EMPTY_GRAPH"
    if [ "$dot" != "$EMPTY_GRAPH" ]; then
        false
    fi
}

# This doesn't detect when a file has been deleted though
# You'd need to check stderr for something like this:
#
# tup warning: generated file 'done\get-a-oven' was deleted outside of tup. This file may be re-created on the next update.
#
# Or stdout for something like this
# $ tup todo
# [ tup ] Scanning filesystem...
# Tup phase 2: The following directories must be parsed:
#  100% .
# Run 'tup parse' to proceed to phase 3.

unfinished_business() {
    tup parse 2>&1 >/dev/null # undocumented
    tup todo 2>&1 | grep -q 'will be executed'
}

tup_fixpoint() {
    while unfinished_business
    do
        echo "There's work to do!!!!!!!"
        tup
        sleep 1s
    done
}

tup_fixpoint

exit 0
