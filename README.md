# Tup to no good

Incremental proofs-of-concept for Tup projects.

The main idea here is exploiting “impure” Tup rules. An impure rule is one that
ignores output to a regular file, particularly when that file is used as the
input to other rules.

I'm interested in this kind of rule two main reasons:

- they can be used to some of the same effects as run scripts, which aren't
  available on Mac OS or Windows.
- they can be used to do things that run scripts can't do, _viz_ using rule
  outputs as inputs to rule generation
  - in other words, second-order rules and beyond

Another concept explored here is the use of a cheap shell loop to get around the
lack of `tup monitor` on non-Linux platforms.
