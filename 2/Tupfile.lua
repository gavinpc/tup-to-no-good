-- The more concise io.lines *will not work* here.  Tup will not notice files
-- opened by io.lines, and thus won't reprocess rules when they change.
local file = assert(io.open(processing_dir..'/'..filename, 'r'))
for line in file:lines() do
  tup.definerule{ command = "echo 'something about "..line.."' > %o", outputs = {'out/'..line} }
end
file:close()
