<?xml version="1.0" encoding="utf-8"?>
<xsl:transform version="1.0"
               xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
               xmlns:notebook="https://gavinpc.com/composition-notebook"
               >
  <xsl:include href="./en-title-to-iso.xsl" />

  <!-- Dump all (internal) wiki links from the given Mediawiki markup. -->
  <xsl:template match="/">
    <mapping>
      <xsl:for-each select="//a[starts-with(@href, './')][not(contains(@href, '#'))]">
        <xsl:variable name="title" select="substring-after(@href, './')" />
        <xsl:variable name="iso" select="notebook:en-title-to-iso($title)" />
        <xsl:if test="$iso != ''">
          <map title="{$title}" iso="{$iso}"/>
        </xsl:if>
      </xsl:for-each>      
    </mapping>

  </xsl:template>

</xsl:transform>
