local file = io.open('checklist', 'r')
if file ~= nil then
  for line in file:lines() do
    tup.definerule{ command = "echo okay I did: "..line.." > %o", outputs = {'done/'..line} }
  end
  file:close()
else
  -- no such file or couldn't open for read
end
