#!/bin/sh

set -e

unfinished_business() {
    tup parse 2>&1 >/dev/null # undocumented
    tup todo 2>&1 | grep -q 'will be executed'
}

tup_fixpoint() {
    while unfinished_business
    do
        tup
    done
}

tup_poll() {
    while true
    do
        tup_fixpoint
        sleep 2s
    done
}

tup_poll

exit 0
